commit 23c91caa5682217ef3a431f7a56575aead02f36c
Merge: 12f977e a1bef94
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Tue Aug 20 12:11:53 2019 -0500

    Merge pull request #14 from regro-cf-autotick-bot/1.0.6
    
    jupyterlab_server v1.0.6

commit a1bef943503a43145ec29bbdccbc4a569f793af5
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Aug 20 16:27:18 2019 +0000

    updated v1.0.6

commit 12f977ef435faa7c28db89deb412208ed8290d9c
Merge: fdb6f36 ef28591
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Tue Aug 20 08:20:34 2019 -0500

    Merge pull request #13 from regro-cf-autotick-bot/1.0.5
    
    jupyterlab_server v1.0.5

commit ef28591c4f65a8e0e924fec8faed25b6a9c1a703
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Tue Aug 20 08:08:44 2019 -0500

    Add jinja2 req

commit 2b1445829581f6d9116265494cd23d63be941820
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Aug 20 11:25:14 2019 +0000

    updated v1.0.5

commit fdb6f364615fdbbd3a4da0e86c4dcde3cbf78cfc
Merge: 613aff9 a5b9d99
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri Aug 16 14:36:15 2019 -0500

    Merge pull request #12 from regro-cf-autotick-bot/1.0.4
    
    jupyterlab_server v1.0.4

commit a5b9d99ef455bfc19df9189f03ef35e4f868ef62
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Aug 16 19:25:38 2019 +0000

    updated v1.0.4

commit 613aff92ef6581cfc6f7b47b776edfefbe8518d5
Merge: f3b9657 1fc7877
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri Aug 16 06:07:39 2019 -0500

    Merge pull request #11 from regro-cf-autotick-bot/1.0.3
    
    jupyterlab_server v1.0.3

commit 1fc7877d3198729a9235bec5a74079b422de24bc
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Aug 16 10:37:28 2019 +0000

    updated v1.0.3

commit f3b9657b79252ffa05a4b6976dee0d40a1f35ea7
Merge: 0b7b916 42a3a41
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Wed Aug 14 14:49:52 2019 -0500

    Merge pull request #10 from regro-cf-autotick-bot/1.0.2
    
    jupyterlab_server v1.0.2

commit 42a3a41d1352c6ffe05f4c8aba9fba00e4325d52
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Aug 14 19:33:01 2019 +0000

    MNT: Re-rendered with conda-build 3.18.9, conda-smithy 3.4.1, and conda-forge-pinning 2019.08.11

commit c9f87e698a8831e9d94c37fa1d551e434f3423a0
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Aug 14 19:32:48 2019 +0000

    updated v1.0.2

commit 0b7b9161055f59a8d3436878059099582b3823f8
Merge: 80378e4 d14dfd2
Author: jakirkham <jakirkham@gmail.com>
Date:   Wed Jul 3 12:50:53 2019 -0400

    Merge pull request #9 from jakirkham-feedstocks/req_jsonschema
    
    Require jsonschema (and add version constraints)

commit d14dfd2901a582febbb3ba61c2ed5f04dc21ae29
Author: John Kirkham <jakirkham@gmail.com>
Date:   Wed Jul 3 11:36:09 2019 -0400

    Bump the build number to 1
    
    Rebuild the package now that the requirements have been updated.

commit 009e404a21915c2154db9d09996640387fca2d7a
Author: John Kirkham <jakirkham@gmail.com>
Date:   Wed Jul 3 11:31:21 2019 -0400

    Add `jsonschema` 3.0.1+ requirement
    
    This requirement was added more recently. So include it here along with
    it's version constraint based on what is listed in the `setup.py`.

commit af7d71d5f833a602190d0a5177fe117e5f95968a
Author: John Kirkham <jakirkham@gmail.com>
Date:   Wed Jul 3 11:30:21 2019 -0400

    Require `notebook` 4.2.0+
    
    Adds the version requirement to `notebook` to match the one in
    `setup.py`.

commit c58d4a167c9a7f9e12bb3b5ff5a13b10b8c992a2
Author: John Kirkham <jakirkham@gmail.com>
Date:   Wed Jul 3 11:28:12 2019 -0400

    Sort dependencies

commit 80378e413eed0c5eee721ea65b7bddfcd152589c
Merge: f3c5d29 c1ab3c6
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri Jun 28 14:37:40 2019 -0500

    Merge pull request #7 from regro-cf-autotick-bot/1.0.0
    
    jupyterlab_server v1.0.0

commit c1ab3c6e7938b41ae492f0eed8307c14c21864b4
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri Jun 28 14:33:34 2019 -0500

    add json5 dep

commit 226bde8fb745763fa08fa5a6962c8c39eb1e237d
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jun 28 19:25:31 2019 +0000

    MNT: Re-rendered with conda-build 3.18.6, conda-smithy 3.4.0, and conda-forge-pinning 2019.06.27

commit 2eb06e1b764d0dd434051a974a806d745a834393
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jun 28 19:25:08 2019 +0000

    updated v1.0.0

commit f3c5d2945205d4b0a490f6c8493993d9fd986422
Merge: 30427bd e6682c1
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Thu Jun 6 05:46:44 2019 -0500

    Merge pull request #6 from regro-cf-autotick-bot/0.3.4
    
    jupyterlab_server v0.3.4

commit e6682c1c82301e6217c9b6f39ddd4d4ec9d7d02a
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Jun 6 10:35:29 2019 +0000

    MNT: Re-rendered with conda-build 3.18.2, conda-smithy 3.3.6, and conda-forge-pinning 2019.06.05

commit 19df960e8665c9cde5a1ed3abfc1fe08334b995c
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Jun 6 10:35:17 2019 +0000

    updated v0.3.4

commit 30427bde0e4a040eafb1099f9a6b0a3e3424e1fa
Merge: 4e377ae f4fc120
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri May 31 12:45:17 2019 -0500

    Merge pull request #5 from regro-cf-autotick-bot/0.3.3
    
    jupyterlab_server v0.3.3

commit f4fc120bf13a78649b0d6ec2c216a0af005f3905
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri May 31 17:37:41 2019 +0000

    updated v0.3.3

commit 4e377ae468c61d9b29243328a877d26a944cb74a
Merge: 71a38aa b827aec
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri May 31 05:40:16 2019 -0500

    Merge pull request #4 from regro-cf-autotick-bot/0.3.2
    
    jupyterlab_server v0.3.2

commit b827aecb8c961d3f1d8ffbcf480d602ad11ab394
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri May 31 10:34:52 2019 +0000

    updated v0.3.2

commit 71a38aa443c0bb6ed7757b52019518bcea2eb583
Merge: c91ad57 38729eb
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Thu May 30 16:40:45 2019 -0500

    Merge pull request #3 from regro-cf-autotick-bot/0.3.1
    
    jupyterlab_server v0.3.1

commit 38729eb827b8db92b7394c8d1a688dc6f96f6505
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu May 30 21:32:54 2019 +0000

    MNT: Re-rendered with conda-build 3.18.2, conda-smithy 3.3.4, and conda-forge-pinning 2019.05.29

commit a8b12002aaf950ee00b9972e876a9dfa5735a75b
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu May 30 21:32:41 2019 +0000

    updated v0.3.1

commit c91ad576589a3a0945dfb580ab764e7aff1f9c38
Merge: 3dfab65 c2f9cc4
Author: Steven Silvester <steven.silvester@ieee.org>
Date:   Fri Feb 15 11:37:34 2019 -0600

    Merge pull request #2 from regro-cf-autotick-bot/0.3.0
    
    jupyterlab_server v0.3.0

commit c2f9cc4a9a3cd74319057019f3279ac47d694395
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Feb 15 17:15:06 2019 +0000

    MNT: Re-rendered with conda-build 3.15.1, conda-smithy 3.2.13, and conda-forge-pinning 2019.02.15

commit dfdb1fcf7d96e48d614acfac6f466d85de23e1eb
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Feb 15 17:14:56 2019 +0000

    updated v0.3.0

commit 3dfab650bf1f959e3d875c5dad2bcedb59db489a
Merge: 6d9a908 cd7d827
Author: Jason Grout <jasongrout@users.noreply.github.com>
Date:   Thu Sep 20 10:49:07 2018 -0700

    Merge pull request #1 from regro-cf-autotick-bot/0.2.0
    
    jupyterlab_server v0.2.0

commit cd7d8271945b8a74a54f8574c8deeddddbedd1a1
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Sep 20 16:21:25 2018 +0000

    updated v0.2.0

commit 6d9a9084e067d37197878a8825d63627dec422be
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Thu Sep 20 15:40:31 2018 +0000

    Re-render the feedstock after CI registration.

commit 31e07d19784a939c1c607b3c19dc662db1b5c44d
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Thu Sep 20 15:25:58 2018 +0000

    Initial feedstock commit with conda-smithy 3.1.12.
