commit 1d0fadc462cb82a89af5ab24cb0894ab50f8c6a1
Merge: 3dbb6b5 6bd4829
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Oct 5 09:38:23 2019 -0400

    Merge pull request #41 from regro-cf-autotick-bot/1.3.9
    
    sqlalchemy v1.3.9

commit 6bd48293991a7e42935c35baeaae2d56438488ea
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Oct 4 21:17:31 2019 +0000

    MNT: Re-rendered with conda-build 3.18.9, conda-smithy 3.5.0, and conda-forge-pinning 2019.10.01

commit de32260889c00927af8f9b0f768f37a791f4e323
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Oct 4 21:17:13 2019 +0000

    updated v1.3.9

commit 3dbb6b50a1a2e4ea59561167fdc92f4cf578329b
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Fri Sep 6 18:57:26 2019 -0500

    [ci skip] ***NO_CI*** Move from appveyor to azure.
    
    See https://github.com/conda-forge/conda-forge.github.io/issues/857

commit 4e41bcef96ea1f25bee2aca4207b532af6a1b35c
Merge: bc13158 953ac24
Author: Igor T. Ghisi <igor.ghisi@gmail.com>
Date:   Tue Aug 27 22:11:05 2019 -0300

    Merge pull request #40 from regro-cf-autotick-bot/1.3.8
    
    sqlalchemy v1.3.8

commit 953ac24d552a06e547fafda349b0de32db7f51a4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Aug 28 00:30:37 2019 +0000

    updated v1.3.8

commit bc1315818b480875bbc6a23727a00068ff70fbcf
Merge: 7d8e1fe a8c301f
Author: Filipe <ocefpaf@gmail.com>
Date:   Wed Aug 14 19:59:27 2019 -0300

    Merge pull request #39 from regro-cf-autotick-bot/1.3.7
    
    sqlalchemy v1.3.7

commit a8c301f71a1f8b70c653219ac76a0ad0f516a05a
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Aug 14 18:25:21 2019 +0000

    updated v1.3.7

commit 7d8e1febda9d0ef7696da788d64025392ff56a1c
Merge: 3bdcb57 2d1f1da
Author: Matt Swain <m.swain@me.com>
Date:   Mon Jul 22 10:52:05 2019 +0100

    Merge pull request #38 from regro-cf-autotick-bot/1.3.6
    
    sqlalchemy v1.3.6

commit 2d1f1da660ae8dd8728af811d73fba1d0684e93a
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Jul 21 21:27:57 2019 +0000

    MNT: Re-rendered with conda-build 3.18.8, conda-smithy 3.4.1, and conda-forge-pinning 2019.07.20

commit 02d52d660fc270ac18adfdcb60a4782486ef595d
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Jul 21 21:27:42 2019 +0000

    updated v1.3.6

commit 3bdcb572aa7abd25347eb52c59ee1045b453e5e7
Merge: 3226afe c369b40
Author: Filipe <ocefpaf@gmail.com>
Date:   Mon Jun 17 14:51:16 2019 -0400

    Merge pull request #37 from regro-cf-autotick-bot/1.3.5
    
    sqlalchemy v1.3.5

commit c369b40e2c0fd932b8923cc2f2e09fa6b26c3911
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Jun 17 18:35:02 2019 +0000

    MNT: Re-rendered with conda-build 3.18.2, conda-smithy 3.3.7, and conda-forge-pinning 2019.06.17

commit 6721539755eb03b89a3e7746cf8ec34453453379
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Jun 17 18:34:48 2019 +0000

    updated v1.3.5

commit 3226afe920cfb6bdb9b5e0fe4e096e1426eb755f
Merge: d6ce957 ddbbf47
Author: Matt Swain <m.swain@me.com>
Date:   Tue May 28 09:47:09 2019 +0100

    Merge pull request #35 from regro-cf-autotick-bot/1.3.4
    
    sqlalchemy v1.3.4

commit ddbbf474aa89772de071096c1822f9a6dfe145ed
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue May 28 02:35:56 2019 +0000

    MNT: Re-rendered with conda-build 3.18.2, conda-smithy 3.3.4, and conda-forge-pinning 2019.05.26

commit ef775b4b71cfaab96640b49f8f9763bd433e8d81
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue May 28 02:35:38 2019 +0000

    updated v1.3.4

commit d6ce9572e204a28e3d30d636b9bcfa593032abfd
Merge: e2d0468 a284dc2
Author: Filipe <ocefpaf@gmail.com>
Date:   Mon Apr 15 14:06:19 2019 -0300

    Merge pull request #32 from regro-cf-autotick-bot/1.3.3
    
    sqlalchemy v1.3.3

commit a284dc2a766e8580839f5053607e3a28e4c4f72c
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Apr 15 16:26:16 2019 +0000

    updated v1.3.3

commit e2d0468a3e6898c8b243de6694e83387306bb848
Merge: 85f7865 9f8354c
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Apr 2 18:12:07 2019 -0300

    Merge pull request #31 from regro-cf-autotick-bot/1.3.2
    
    sqlalchemy v1.3.2

commit 9f8354c853ca3c03f629174b8426cb3f91af794b
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Apr 2 18:28:44 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.3.2, and conda-forge-pinning 2019.03.17

commit f3a7cbbdc31536fbd872845a42414de35f2b217e
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Apr 2 18:28:31 2019 +0000

    updated v1.3.2

commit 85f7865f537f79002d4e610630a066412be9853c
Merge: f9a883c e07f78e
Author: Filipe <ocefpaf@gmail.com>
Date:   Sat Mar 9 20:29:18 2019 -0300

    Merge pull request #30 from regro-cf-autotick-bot/1.3.1
    
    sqlalchemy v1.3.1

commit e07f78e17aae328485985fec06c09c5f1348edde
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Mar 9 20:25:47 2019 +0000

    updated v1.3.1

commit f9a883c2f0e07ee7536b6d53b1ceb83eeaa92998
Merge: aa60104 b56f94c
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Tue Mar 5 10:34:49 2019 +0530

    Merge pull request #29 from regro-cf-autotick-bot/1.3.0
    
    sqlalchemy v1.3.0

commit b56f94c06c5bb496dc6c38bb444b24ded4ade150
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Mar 4 22:24:50 2019 +0000

    updated v1.3.0

commit aa6010413583ddc61c1e7c31aca464e11536f191
Merge: a05253c 7b88c01
Author: Marius van Niekerk <marius.v.niekerk@gmail.com>
Date:   Sat Mar 2 00:42:28 2019 -0500

    Merge pull request #28 from regro-cf-autotick-bot/rebuildaarch64_and_ppc64le_addition1_arch
    
    Arch Migrator

commit 7b88c01c6a125a42997d2b413824b51b761aab36
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Mar 2 05:27:57 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.2.14, and conda-forge-pinning 2019.02.24

commit e0c5adb24ae9f79c287a85450387846af1d4b3a6
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Mar 2 05:27:45 2019 +0000

    bump build number

commit a05253cb7ba0b8c61a7aaccd9e53e93726ffd912
Merge: b306e9e 03dca52
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Feb 16 02:41:32 2019 +0530

    Merge pull request #27 from regro-cf-autotick-bot/1.2.18
    
    sqlalchemy v1.2.18

commit 03dca52f01183e8709d86aa538f6b230ea75c4b6
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Feb 15 18:15:27 2019 +0000

    MNT: Re-rendered with conda-build 3.15.1, conda-smithy 3.2.13, and conda-forge-pinning 2019.02.15

commit 7889277c0fbc2448caac992d684893de6c1bbe28
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Feb 15 18:15:15 2019 +0000

    updated v1.2.18

commit b306e9e89d4dff14f16661ab056332c97574a0da
Merge: 1bc4f62 e8bdbb0
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Jan 29 08:36:29 2019 -0200

    Merge pull request #26 from regro-cf-autotick-bot/1.2.17
    
    sqlalchemy v1.2.17

commit e8bdbb0a78ef525b937a97a197eccfc6aaa663c4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jan 29 04:53:09 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.2.10, and conda-forge-pinning 2019.01.21

commit a048e82aa7df5346080cf309226bde3db1afc6b4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jan 29 04:52:57 2019 +0000

    updated v1.2.17

commit 1bc4f622d2f82cf27a2b026ee96bbeba19c1d417
Merge: 5a3b477 c00f54c
Author: Filipe <ocefpaf@gmail.com>
Date:   Fri Jan 11 19:54:10 2019 -0200

    Merge pull request #25 from regro-cf-autotick-bot/1.2.16
    
    sqlalchemy v1.2.16

commit c00f54cf18b90bcccb23cea5d393f6ec1b163bb9
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jan 11 15:23:57 2019 +0000

    MNT: Re-rendered with conda-smithy 3.2.2 and pinning 2019.01.09

commit 06bc29fa7e06028f82f9867dfeef61af83187b88
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jan 11 15:23:49 2019 +0000

    updated v1.2.16

commit 5a3b477abe57d60e01bc7a7ab4a38624885e546d
Merge: a1948f2 acb7b81
Author: Matt Swain <m.swain@me.com>
Date:   Wed Dec 12 10:04:54 2018 +0000

    Merge pull request #24 from regro-cf-autotick-bot/1.2.15
    
    sqlalchemy v1.2.15

commit acb7b8154ff7780be4555a7171ae947aa066e2c5
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Dec 11 22:14:48 2018 +0000

    updated v1.2.15

commit a1948f2b8c6a90f976b9a17d339a8ed7c9f305f4
Merge: 55b04b8 e91486c
Author: Filipe <ocefpaf@gmail.com>
Date:   Sun Nov 11 19:54:19 2018 -0200

    Merge pull request #23 from regro-cf-autotick-bot/1.2.14
    
    sqlalchemy v1.2.14

commit e91486c5d0d2cce504e2df1cd951c5639b503d44
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Nov 10 21:14:11 2018 +0000

    updated v1.2.14

commit 55b04b88ea8a1113199f69e526d5a2ef75f7689b
Merge: 0eb84dc d457ca1
Author: Matt Swain <m.swain@me.com>
Date:   Thu Nov 1 09:41:52 2018 +0000

    Merge pull request #22 from regro-cf-autotick-bot/1.2.13
    
    sqlalchemy v1.2.13

commit d457ca10ce60de99c36d2ee13376d149da13cec1
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Oct 31 21:15:15 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.10.31

commit 8fc5e0304a4dd149daa7870a3b7a7c3d7872f23f
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Oct 31 21:15:10 2018 +0000

    updated v1.2.13

commit 0eb84dc460adcd5505dd9ac4fa4bd61cb63c3d82
Merge: 8f6295c 5e88128
Author: Marius van Niekerk <marius.v.niekerk@gmail.com>
Date:   Wed Oct 3 07:32:10 2018 -0400

    Merge pull request #21 from regro-cf-autotick-bot/rebuild
    
    Rebuild for Python 3.7, GCC 7, R 3.5.1, openBLAS 0.3.2

commit 5e88128ba9e9ddddb33fb48a73536e3b61f1f542
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Oct 2 23:12:17 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.10.01

commit 636394e5627e2e2ec57a66b7d17cc6a8ecace7bc
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Oct 2 23:12:10 2018 +0000

    bump build number

commit 8f6295c408d9047870c07ec72434aecadbf6614f
Merge: 8133919 471cec5
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Sep 20 07:57:27 2018 -0300

    Merge pull request #19 from regro-cf-autotick-bot/1.2.12
    
    sqlalchemy v1.2.12

commit 471cec5698158f393b9377da47b526609841d952
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Sep 19 18:23:34 2018 +0000

    updated v1.2.12

commit 8133919c4651e22d10fa8fee548e96ebbacf0096
Merge: 36ccea3 887445c
Author: Matt Swain <m.swain@me.com>
Date:   Tue Aug 21 13:41:24 2018 +0100

    Merge pull request #17 from regro-cf-autotick-bot/1.2.11
    
    sqlalchemy v1.2.11

commit 887445c8c2702e22429f08e5e7a2bc784d9bc8e4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Aug 21 02:16:37 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.08.14

commit 14cf2144cd798e8f6dc8bc2b395e543540684f6b
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Aug 21 02:16:31 2018 +0000

    updated v1.2.11

commit 36ccea300ddf67102e75eeffdcfa6ddd58ea286f
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 19:13:30 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit 76776499227ecc69df57189adbe6114798570ba2
Merge: ad9f5b9 8609055
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Jul 26 21:24:02 2018 -0300

    Merge pull request #15 from ocefpaf/update_recipe
    
    update recipe to the new format

commit 8609055b0220655094d0448f843d8487f20738f2
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Thu Jul 26 19:04:51 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.8 and pinning 2018.07.24

commit e0fb184231c4a586dfd621eba8efb306db66c445
Author: Filipe Fernandes <ocefpaf@gmail.com>
Date:   Thu Jul 26 16:04:12 2018 -0300

    update recipe to the new format

commit ad9f5b95189fda2976ae29db88749a4b5eaef323
Merge: 7478c8c 2f13f07
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sun Jul 15 19:34:10 2018 +0530

    Merge pull request #13 from regro-cf-autotick-bot/1.2.10
    
    sqlalchemy v1.2.10

commit 2f13f07aabf010341583dd2c88836819d3fab59e
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jul 13 23:08:53 2018 +0000

    updated v1.2.10

commit 7478c8c32e7af27f8653c75569eb1b07b5f66f70
Merge: 28f4234 0c8a4be
Author: Filipe <ocefpaf@gmail.com>
Date:   Sun Jul 1 16:53:47 2018 -0300

    Merge pull request #12 from regro-cf-autotick-bot/1.2.9
    
    sqlalchemy v1.2.9

commit 0c8a4bea1a9d1e06490250a7a9baac0abf0f6aa9
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Jul 1 13:59:01 2018 +0000

    updated v1.2.9

commit 28f423407b12a9c3003bca00e754a3cfc5d3a758
Merge: 8c4c838 8bc4678
Author: Matt Swain <m.swain@me.com>
Date:   Tue May 29 09:25:41 2018 +0100

    Merge pull request #11 from regro-cf-autotick-bot/1.2.8
    
    sqlalchemy v1.2.8

commit 8bc4678fc1e9efd88fff24057d4df94d4f728744
Author: Travis CI User <travis@example.org>
Date:   Mon May 28 23:16:47 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.5 and pinning 2018.05.22

commit 0b393cea00928595186d5fbb6971325dac333f0c
Author: Travis CI User <travis@example.org>
Date:   Mon May 28 23:16:21 2018 +0000

    updated v1.2.8

commit 8c4c838223aa05f2c879220d73145456aebfbd55
Merge: 85aa90f 58b27de
Author: Filipe <ocefpaf@gmail.com>
Date:   Mon Apr 23 16:17:59 2018 -0300

    Merge pull request #10 from regro-cf-autotick-bot/1.2.7
    
    sqlalchemy v1.2.7

commit 58b27de1497a3cf7b027a5342d7c20a5eba79680
Author: Travis CI User <travis@example.org>
Date:   Mon Apr 23 17:29:00 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.2 and pinning 2018.04.19

commit 46025c6a8296248dbcfd784f1e96da280c8eb080
Author: Travis CI User <travis@example.org>
Date:   Mon Apr 23 17:28:19 2018 +0000

    updated v1.2.7

commit 85aa90fe261694eec61448cd8fdfbf9e910e98d2
Merge: 1d3145c 2647bce
Author: Igor T. Ghisi <igor.ghisi@gmail.com>
Date:   Mon Apr 2 23:40:50 2018 -0300

    Merge pull request #9 from regro-cf-autotick-bot/1.2.6
    
    sqlalchemy v1.2.6

commit 2647bce66d640244f0f906471aa8a4c4af3539a3
Author: Travis CI User <travis@example.org>
Date:   Mon Apr 2 23:41:14 2018 +0000

    updated v1.2.6

commit 1d3145ccc24244e564bb1cc31b7ce14ce290abf6
Merge: 743a9a9 5f51628
Author: Filipe <ocefpaf@gmail.com>
Date:   Wed Mar 7 08:30:31 2018 -0300

    Merge pull request #8 from regro-cf-autotick-bot/1.2.5
    
    sqlalchemy v1.2.5

commit 5f516285bde91f5bdbc4f963533f39e189e42fb1
Author: Travis CI User <travis@example.org>
Date:   Tue Mar 6 22:20:06 2018 +0000

    updated v1.2.5

commit 743a9a96500e6abb0effd7ea9e6461ebc67ee1fa
Merge: c89a40e 05af1e2
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Wed Mar 7 01:06:14 2018 +0530

    Merge pull request #7 from regro-cf-autotick-bot/1.2.4
    
    sqlalchemy v1.2.4

commit 05af1e22036e2e706cd70f0d4869b00b9a360ff3
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Mar 6 08:03:18 2018 -0300

    Update meta.yaml

commit 3b5921d16e45c8ef8ffc1309fc83e487a82c8da4
Author: Travis CI User <travis@example.org>
Date:   Tue Mar 6 03:25:58 2018 +0000

    updated v1.2.4

commit c89a40e4510d06e076cf39a17f57599cd2c0099c
Merge: fd8a5b3 3759503
Author: Filipe <ocefpaf@gmail.com>
Date:   Mon Jan 22 09:15:20 2018 -0200

    Merge pull request #6 from igortg/patch-1
    
    Bump to version 1.12.1

commit 375950349047fa5d26c89fb136da4caf6bc84745
Author: Igor T. Ghisi <igor.ghisi@gmail.com>
Date:   Sat Jan 20 19:31:18 2018 -0200

    Update maintainers list

commit 13357faab891a47f663e324edf3612c45bc352e9
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Fri Jan 19 18:41:27 2018 +0000

    MNT: Re-rendered with conda-smithy 2.4.5

commit f456c496b1eaca089229ef0ab1786bc18634d3a4
Author: Igor T. Ghisi <igor.ghisi@gmail.com>
Date:   Fri Jan 19 15:56:27 2018 -0200

    Bump to version 1.12.1

commit fd8a5b35314c285280da2c2bf55a4afaa2d24df7
Merge: 93bb679 a2189b7
Author: Matt Swain <m.swain@me.com>
Date:   Thu Aug 17 11:04:16 2017 +0100

    Merge pull request #5 from nehaljwani/bump-version-1.1.13
    
    Bump version to 1.1.13

commit 93bb6799afbe68ccdd297be24b077440b52b2440
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue Aug 15 01:27:34 2017 +0000

    Re-render the feedstock after CI registration.

commit a2189b7f3af92fc0133f38c3394b92e19a802036
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sun Aug 13 17:30:50 2017 +0000

    Bump version to 1.1.13

commit 805786de11296e65d25b8008617ff7ff96c563ad
Merge: 4d079f9 3f65426
Author: Filipe <ocefpaf@gmail.com>
Date:   Wed Jun 28 13:52:43 2017 -0300

    Merge pull request #4 from mcs07/master
    
    Update to 1.1.11 and re-render

commit 3f654260dbce66fcd47c9f2e54128707d6acffc5
Author: Matt Swain <m.swain@vernalis.com>
Date:   Wed Jun 28 15:57:43 2017 +0100

    Add myself as a maintainer

commit e04ea55297dc4770f66b95e3df66f92e36b05f1c
Author: Matt Swain <m.swain@vernalis.com>
Date:   Wed Jun 28 15:19:46 2017 +0100

    Update to 1.1.11

commit a0ea26c8e5f5315f5fdc9eeb85cd7b406fbc52cf
Author: Matt Swain <m.swain@vernalis.com>
Date:   Wed Jun 28 15:14:32 2017 +0100

    MNT: Re-rendered with conda-smithy 2.3.2

commit 85ffdffe9d6f759036f044baded1231cac27154e
Author: Matt Swain <m.swain@vernalis.com>
Date:   Mon May 22 13:33:16 2017 +0100

    Update to 1.1.10

commit 4d079f92f789f2eadd10d3a1f26c4c52436b5cf2
Merge: 30156c6 63be563
Author: Filipe <ocefpaf@gmail.com>
Date:   Fri Feb 24 07:33:25 2017 -0300

    Merge pull request #2 from pmlandwehr/master
    
    MNT: Re-rendered with conda-smithy 2.0.1

commit 63be5635ea27a7d33d5d2d75d912ee0331eb3918
Author: Pete[r] M. Landwehr <pmlandwehr@gmail.com>
Date:   Thu Feb 16 16:28:48 2017 -0800

    MNT: Re-rendered with conda-smithy 2.0.1

commit 30156c693f40311d9a952fd8a257a2b32ea38c69
Merge: 3f82ff7 0d5c3d5
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Jan 26 08:42:55 2017 -0300

    Merge pull request #1 from ocefpaf/update
    
    Update

commit 0d5c3d5c8fbc62e2045f859a851b8fe237bd1d9f
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Wed Jan 25 09:26:04 2017 -0300

    update to 1.1.5

commit 792e7b0de10bbe0706d1fb2d63f9e20cf68baffe
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Wed Jan 25 09:25:57 2017 -0300

    rerender with auto

commit 3f82ff7d436e3b6a55b3ff8d0e73ce521dd249d6
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Mon Jan 2 21:48:32 2017 +0000

    Re-render the feedstock after CI registration.

commit ae4a24ce515615c0c7ec7978fbcd2e3eef975acd
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Mon Jan 2 21:45:32 2017 +0000

    Initial feedstock commit with conda-smithy 1.7.0.
