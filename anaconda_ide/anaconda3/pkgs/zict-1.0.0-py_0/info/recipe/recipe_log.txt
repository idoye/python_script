commit 98782f0457f84a10b78c86ac8ae01e9fd20791ed
Author: Maggie-M <maggie.mari@continuum.io>
Date:   Mon Sep 18 23:30:15 2017 -0500

    Update about section

commit 0bdc228388f6bc92e4d461444819f74295539115
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Thu Jul 13 12:03:00 2017 +0000

    Make recipe cross-compile capable

commit 666abecbf975812172854bef595d38e2c2e01466
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jun 25 17:34:09 2019 +0000

    MNT: Re-rendered with conda-build 3.18.4, conda-smithy 3.4.0, and conda-forge-pinning 2019.06.19

commit 2fd47bbea8defe77351e2df842a7d153535130a4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jun 25 17:33:57 2019 +0000

    updated v1.0.0

commit f13808ca22ec3135f2b5bf4c910872ccd82e4192
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Mar 9 17:28:56 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.2.14, and conda-forge-pinning 2019.03.04

commit 2a5523ce0657395bed0e40b76f1325a1230b21cf
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Mar 9 17:28:45 2019 +0000

    updated v0.1.4

commit 81bd987f33c36638c15605d93efc5a2691d1cbff
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 17:31:41 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit a47aed5229cce3681572966e9be0b56e21c3df51
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed Sep 20 11:31:35 2017 -0400

    MNT: Re-rendered with conda-smithy 2.4.0

commit 0fdb1a70e19542411212aadf4a3da303f9190367
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed Sep 20 11:29:29 2017 -0400

    add noarch: python to build

commit 5a2b32eb790c8d85bbc956eb54fac97153d7a609
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed Sep 20 09:35:19 2017 -0400

    bump zict to 0.1.3

commit 4cfa49a833282a2c72567c00184a8dc6e8e5deb0
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed Sep 20 09:33:37 2017 -0400

    MNT: Re-rendered with conda-smithy 2.4.0

commit 85c79218337c937c6825b9572b8999acbd99903b
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Aug 26 22:12:12 2017 +0000

    [ci skip] [skip ci] Update anaconda token

commit 0df42497263b142449f6e0c693ef72a22d706b6b
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed May 3 17:27:16 2017 -0400

    add license_file entry to meta.yaml

commit 3ad82cd7756c52104fcb24ff7856653a579e78de
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed May 3 13:44:12 2017 -0400

    bump to version 0.1.2

commit e3a52968c2d6a90714ef45b1c8b97737c731987e
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed May 3 13:43:45 2017 -0400

    MNT: Re-rendered with conda-smithy 2.3.0

commit 2edfa751b0263fdccb84abfcb0b5fbeab5db5aeb
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Sun Jan 1 18:59:54 2017 -0500

    MNT: Re-rendered with conda-smithy 1.7.0 (#5)

commit cf2f8f137010c3092ed38706afbc18e852cb1c65
Merge: cc78538 3ab584a
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Sat Dec 31 18:53:10 2016 -0500

    Merge pull request #4 from mrocklin/bump-0.1.1
    
    bump version to 0.1.1

commit 3ab584a3a416c59be67310f9bc45935544334066
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Wed Dec 28 10:43:30 2016 -0500

    bump version to 0.1.1

commit cc7853865d10577bea45a4e3f26c536bb61c0410
Merge: 9689f79 8f1d696
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Fri Nov 4 19:38:57 2016 -0400

    Merge pull request #2 from mrocklin/version-0.1.0
    
    Version 0.1.0

commit 8f1d696488046b628b70f5deae28fcee78cc89c0
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Fri Nov 4 11:59:26 2016 -0400

    bump version to 0.1.0

commit 13eb6f319195bae2fc3ede51cd0a15f76921ffad
Author: Matthew Rocklin <mrocklin@gmail.com>
Date:   Fri Nov 4 11:58:51 2016 -0400

    MNT: Re-rendered with conda-smithy 1.4.6

commit 9689f79058c2a5e23d42931a03f3230188ed57b7
Merge: 2844635 9704f07
Author: jakirkham <jakirkham@gmail.com>
Date:   Sat Oct 22 03:26:48 2016 -0400

    Merge pull request #1 from conda-forge-admin/feedstock_rerender_master
    
    MNT: Re-render the feedstock [ci skip]

commit 9704f07c2957c6909466236e48659b74e41b8de9
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Sat Oct 22 01:29:09 2016 +0000

    MNT: Updated the feedstock for conda-smithy version 1.3.3.

commit 28446353b1fa7c1eb8e61dc622125484d09a6bf4
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Mon Sep 12 10:59:37 2016 +0000

    Re-render the feedstock after CI registration.

commit c69bdf0e98a7b56b94045ce0e0a8ad80629744ce
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Mon Sep 12 10:58:01 2016 +0000

    Initial commit of the zict feedstock.
