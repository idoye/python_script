commit ae166f760639d622eb5fbcf69dbd0b9db947470d
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Mon Aug 14 02:00:34 2017 -0500

    Update about section

commit 3d4f690f732edb856977f327f930cfc8fa5549de
Merge: d3bd74b 338a51b
Author: Joshua Adelman <synapticarbors@users.noreply.github.com>
Date:   Mon Apr 29 13:51:39 2019 -0400

    Merge pull request #6 from regro-cf-autotick-bot/1.1.0
    
    fastcache v1.1.0

commit 338a51bce1329443c8174f53399507823a03becd
Author: Joshua Adelman <synapticarbors@users.noreply.github.com>
Date:   Mon Apr 29 13:38:23 2019 -0400

    Update meta

commit e41b03eb04ab77fc07c20e099e38d4ddf2979678
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Apr 29 17:31:40 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.3.4, and conda-forge-pinning 2019.04.25

commit dad28eed5995081bec2e02cd86fc27b93c2b9c50
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Apr 29 17:31:28 2019 +0000

    updated v1.1.0

commit d3bd74bd3e2888381662e11d453c0541b2193aec
Merge: 818d4ca 6fdf02c
Author: Joshua Adelman <synapticarbors@users.noreply.github.com>
Date:   Wed Oct 3 07:50:10 2018 -0400

    Merge pull request #4 from regro-cf-autotick-bot/rebuild
    
    Rebuild for Python 3.7, GCC 7, R 3.5.1, openBLAS 0.3.2

commit 6fdf02c93758f5c14c6e2b5ee629e551bfd0622e
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Oct 3 00:16:56 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.10.01

commit e7e063deb4bff118249291714a5e16886fadc296
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Oct 3 00:16:48 2018 +0000

    bump build number

commit 818d4ca223532ff2a739083d2254cc36db9fcd64
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 19:30:56 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit 19a9339a5f747fdca68bd76467439bf7b5605edc
Merge: f268ebb 1562bf4
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Jul 31 22:09:41 2018 -0300

    Merge pull request #3 from ocefpaf/py37
    
    testing py37

commit 1562bf459518aaff30978f203bae63c2ec5591a1
Author: Filipe Fernandes <ocefpaf@gmail.com>
Date:   Tue Jul 31 19:35:40 2018 -0300

    testing py37

commit f268ebbeac7ae826cbb4b88f877479ef0f4738cd
Merge: ac6f2f6 0f500d9
Author: Joshua Adelman <synapticarbors@users.noreply.github.com>
Date:   Mon Jun 25 10:50:37 2018 -0400

    Merge pull request #1 from regro-cf-autotick-bot/compiler_migration2
    
    Migrate to Jinja2 compiler syntax

commit 0f500d9cce27dc96d9a446f38db52c040efaa8ab
Author: Travis CI User <travis@example.org>
Date:   Sat Jun 23 01:20:46 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.6 and pinning 2018.06.19

commit 3c7275e6c7102953feea6d9827847de041d6634e
Author: Travis CI User <travis@example.org>
Date:   Sat Jun 23 01:20:40 2018 +0000

    migrated to Jinja2 compiler syntax build

commit ac6f2f6842b637d53302536f0c8a46465a540d0d
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Aug 26 22:00:55 2017 +0000

    [ci skip] [skip ci] Update anaconda token

commit 7b7e38e2b2298e101d9e3bee386b06dfb0029bcc
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Wed Jun 21 04:18:08 2017 +0000

    Re-render the feedstock after CI registration.

commit 6e24e2a024c2e146f8da1b360de78b1176c579ae
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Wed Jun 21 04:15:37 2017 +0000

    Initial feedstock commit with conda-smithy 2.3.2.
