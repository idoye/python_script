commit 898a8972a00ce86ae2c7982b14b195ce094ece6a
Author: Jonathan Helmus <jjhelmus@gmail.com>
Date:   Mon Jun 25 11:38:16 2018 -0500

    make recipe cross-compile capable

commit 62287458baac3b9bf720e9b0a087835736fd0a88
Merge: 3586def 6d05eec
Author: Filipe <ocefpaf@gmail.com>
Date:   Wed Feb 13 16:47:37 2019 -0200

    Merge pull request #20 from regro-cf-autotick-bot/0.13.2
    
    joblib v0.13.2

commit 6d05eec4effcf932155391a4471c7f36804842a3
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Feb 13 17:16:20 2019 +0000

    MNT: Re-rendered with conda-build 3.15.1, conda-smithy 3.2.13, and conda-forge-pinning 2019.02.11

commit 8b77f95d3c625b7a377dae73a244d4d56b834008
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Feb 13 17:16:08 2019 +0000

    updated v0.13.2

commit 3586defc688b2e46390848fddd368d7c3b61bd75
Merge: af8c3f9 036bfc8
Author: Filipe <ocefpaf@gmail.com>
Date:   Fri Jan 11 19:52:22 2019 -0200

    Merge pull request #19 from regro-cf-autotick-bot/0.13.1
    
    joblib v0.13.1

commit 036bfc8bb0ce792c767dcd02626a3e70f8a9bf17
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jan 11 19:19:21 2019 +0000

    MNT: Re-rendered with conda-smithy 3.2.2 and pinning 2019.01.09

commit dfbad8963d4209ca6a05e53ad045591db3bdac2b
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Jan 11 19:19:16 2019 +0000

    updated v0.13.1

commit af8c3f907918fe9ca9b1d4fa6717690e2ebf2820
Merge: b8c1ecf 71d0823
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Nov 6 15:53:19 2018 -0200

    Merge pull request #18 from regro-cf-autotick-bot/0.13.0
    
    joblib v0.13.0

commit 71d082380cdb9aa4f1ded3461c0599d76a63046a
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Nov 6 17:16:38 2018 +0000

    updated v0.13.0

commit b8c1ecf6bb82605e2a93cf1da2a1f219df609d4a
Merge: e4f9ab1 b64f318
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Sep 13 12:03:16 2018 -0300

    Merge pull request #17 from regro-cf-autotick-bot/0.12.5
    
    joblib v0.12.5

commit b64f3188702cc4049a87b2a2471d4465b13b0551
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Sep 13 14:14:56 2018 +0000

    updated v0.12.5

commit e4f9ab19bf87838fae85100471fd33e73d47be19
Merge: 22ead6b e0c6aaf
Author: Filipe <ocefpaf@gmail.com>
Date:   Wed Sep 5 08:46:20 2018 -0300

    Merge pull request #16 from regro-cf-autotick-bot/0.12.4
    
    joblib v0.12.4

commit e0c6aaf1b71fce13ccfdfc41a0bca0455e0894e5
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Wed Sep 5 10:11:56 2018 +0000

    updated v0.12.4

commit 22ead6b6f0370c750d88d24e38c9437a906b0de8
Merge: 90db98d b0d607d
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Aug 30 12:17:17 2018 -0300

    Merge pull request #15 from regro-cf-autotick-bot/0.12.3
    
    joblib v0.12.3

commit b0d607d3cdb7b403bee106ee6f8d155f2538f72f
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Aug 30 12:06:09 2018 -0300

    Update meta.yaml

commit a7d982ae586d7eb19b2d1bf3b01b6566c511868c
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Aug 30 14:11:31 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.08.28

commit a17115922397a62c3adde069b2db50197d115f3f
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Aug 30 14:11:08 2018 +0000

    updated v0.12.3

commit 90db98d0f04b4a924749b97649b048c5a68e3f73
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 17:24:13 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit 931160651b3d47c3841b038083ddf94760e23607
Merge: 6288b65 f45729e
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Aug 2 10:19:39 2018 -0300

    Merge pull request #14 from regro-cf-autotick-bot/0.12.2
    
    joblib v0.12.2

commit f45729e8c3515eb75a68accaf844774fea60ae4b
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Aug 2 13:15:43 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.10 and pinning 2018.07.24

commit b0216b0b3c58586c5c7508913848c4050c12470a
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Thu Aug 2 13:15:37 2018 +0000

    updated v0.12.2

commit 6288b652474b09c86a13bb2992476580bc0e4b1b
Merge: d34d561 930246e
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Jul 17 11:14:19 2018 -0500

    Merge pull request #13 from regro-cf-autotick-bot/0.12.1
    
    joblib v0.12.1

commit 930246e8712578fb55e60f79c08fb96e584d9af5
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jul 17 16:10:57 2018 +0000

    updated v0.12.1

commit d34d561906434033586890b7d665aaf624984bd5
Merge: b2c0318 70ff295
Author: jakirkham <jakirkham@gmail.com>
Date:   Sat Jun 23 00:49:41 2018 -0400

    Merge pull request #12 from jakirkham-feedstocks/release_0.12
    
    Release 0.12

commit 70ff295a44ffeb0cbdd3c420168a4525586d5be1
Author: John Kirkham <kirkhamj@janelia.hhmi.org>
Date:   Sat Jun 23 00:34:04 2018 -0400

    Release 0.12

commit ed991b41615f0d4cfc5df193abca8cfe4a6187e7
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Sat Jun 23 04:32:42 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.6 and pinning 2018.06.19

commit 319dd441fada273ddcc7aa2c182895c44b36d511
Author: John Kirkham <kirkhamj@janelia.hhmi.org>
Date:   Sat Jun 23 00:30:47 2018 -0400

    Switch to noarch Python
    
    As this is a pure Python package with no conditional dependencies,
    switch it over to `noarch: python`. Should cutdown the maintenance
    burden associated with keeping this feedstock up-to-date.

commit 9bed7111d61238e0246b763f0f95b30aa00cb717
Author: John Kirkham <kirkhamj@janelia.hhmi.org>
Date:   Sat Jun 23 00:29:35 2018 -0400

    Use pip
    
    Make use of `pip` to do the build and install per conda-forge best
    practices.

commit b2c03183220eb9c32deeba9829ade6f8f836a4b4
Merge: 7d627a2 fc8ace2
Author: jakirkham <jakirkham@gmail.com>
Date:   Sat Jun 23 00:28:09 2018 -0400

    Merge pull request #11 from conda-forge-linter/conda_forge_admin_10
    
    MNT: rerender

commit fc8ace2d84de97f6fd235ba8b28303aea0a5435f
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Fri Jun 22 23:04:31 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.6 and pinning 2018.06.19

commit 7d627a2be139a9446d2bba8e361f6c73a41bff66
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Aug 26 20:07:14 2017 +0000

    [ci skip] [skip ci] Update anaconda token

commit 656bb4f6d8ceb8db490a469ff653de479d35f617
Merge: 915eeb2 bb8a6da
Author: Filipe <ocefpaf@gmail.com>
Date:   Fri Mar 3 11:40:57 2017 -0300

    Merge pull request #9 from ocefpaf/update_to_0.11
    
    Update to 0.11

commit bb8a6da723d36af1cc5a2ff0dfd319ba7da84197
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Thu Mar 2 14:40:19 2017 -0300

    Update to 0.11

commit 915eeb2a4c185caf028d3d0af617d0a18d5cd7c4
Merge: 8e23baa 209141d
Author: jakirkham <jakirkham@gmail.com>
Date:   Sun Feb 26 19:08:34 2017 -0500

    Merge pull request #8 from conda-forge-admin/feedstock_rerender_master
    
    MNT: Re-render the feedstock [ci skip]

commit 209141daee46a729781e203f854f3d88c43a266e
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Sun Feb 26 20:57:26 2017 +0000

    MNT: Updated the feedstock for conda-smithy version 2.1.0.

commit 8e23baa8a574653ef3d62e27fb2f8d36578bef8b
Merge: ccba060 0ea8493
Author: jakirkham <jakirkham@gmail.com>
Date:   Sun Jan 22 17:05:11 2017 -0500

    Merge pull request #7 from conda-forge-admin/feedstock_rerender_master
    
    MNT: Re-render the feedstock

commit 0ea84938aa7d4bd3dabfe0625a3ebb7de017fa9a
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Wed Jan 18 01:43:29 2017 +0000

    MNT: Updated the feedstock for conda-smithy version 2.0.0.

commit ccba060d87db044a9de4519660dc8bd797dd7dca
Merge: 380c656 186a6e4
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Oct 25 15:28:02 2016 -0300

    Merge pull request #6 from ocefpaf/update
    
    Update

commit 186a6e4f61fae8b6c08f9beaa682210871100d5b
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Tue Oct 25 12:19:40 2016 -0300

    Updated to 0.10.3

commit 636018f235f956b2e544e235290df0938ab9d26a
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Tue Oct 25 12:19:34 2016 -0300

    rerender

commit 380c656242e936951ef012e1466110a89c198a3c
Merge: 1ea736a f891d77
Author: Filipe <ocefpaf@gmail.com>
Date:   Thu Oct 6 09:22:01 2016 -0300

    Merge pull request #5 from conda-forge-admin/feedstock_rerender_master
    
    MNT: Re-render the feedstock [ci skip]

commit f891d77a53e5e37a45697560a3b5ee23d77a3a64
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Thu Oct 6 07:37:11 2016 +0000

    MNT: Updated the feedstock for conda-smithy version 1.3.2.

commit 1ea736a7b9cb1d4308a715a0bdaa4e155b940e84
Merge: 75b4342 8f4d248
Author: jakirkham <jakirkham@gmail.com>
Date:   Sun Sep 18 23:07:10 2016 -0400

    Merge pull request #4 from conda-forge-admin/feedstock_rerender_master
    
    MNT: Re-render the feedstock

commit 8f4d24820e21e812cee3a806a3aeefd4ddab1cde
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Mon Sep 19 01:34:30 2016 +0000

    MNT: Updated the feedstock for conda-smithy version 1.2.0.
    
    [ci skip]

commit 75b4342a0e4cff958011fcdd919e3ac073350b84
Merge: fff691c 9501ff8
Author: Filipe <ocefpaf@gmail.com>
Date:   Sat Sep 3 09:43:35 2016 -0300

    Merge pull request #2 from ocefpaf/bump
    
    Bump

commit 9501ff89e260fdfd0ba1c9ebd68d5ba829735739
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Fri Sep 2 21:03:09 2016 -0300

    Update to 0.10.2

commit 4b24b89cadabadeae8c7037b8b00a7e5ba977328
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Fri Sep 2 21:02:56 2016 -0300

    rerender

commit fff691c65462b50ec24182b8c44c5921716adabd
Merge: 6e4f70d 1984168
Author: Filipe <ocefpaf@gmail.com>
Date:   Tue Jul 26 09:55:26 2016 -0300

    Merge pull request #1 from ocefpaf/update
    
    Update to 0.10.0

commit 19841683d97b14319753c7b425fef1cad97ca432
Author: ocefpaf <ocefpaf@gmail.com>
Date:   Sun Jul 24 19:13:01 2016 -0300

    Update to 0.10.0

commit 6e4f70de9dc4a504c2f058714de70732f3625992
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sun Jul 24 22:07:44 2016 +0000

    Re-render the feedstock after CI registration.

commit 2507d96f98e21f91579d51aeb9425c42f90973c9
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sun Jul 24 22:06:38 2016 +0000

    Initial commit of the joblib feedstock.
