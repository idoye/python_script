commit 03d65706bdaed3a6ca310b5bae77da4b4000ee52
Author: Jonathan Helmus <jjhelmus@gmail.com>
Date:   Tue Apr 24 09:28:10 2018 -0500

    remove unnecessary host requirements

commit fc310ce8e41e0d088fddf4c22c287b6492f40cdb
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Thu Feb 15 15:38:38 2018 -0600

    Make recipe cross compile capable

commit 475d9d26f3aa6c29014a239b80643714e3292233
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Thu Feb 15 15:38:34 2018 -0600

    Remove noarch for now

commit 7382b7287d22039e5272546abad9e8fc9f4726c5
Merge: f19b73f 523fc28
Author: Thomas Robitaille (conda-forge account) <34445423+astrofrog-conda-forge@users.noreply.github.com>
Date:   Mon Oct 29 14:52:07 2018 +0000

    Merge pull request #5 from regro-cf-autotick-bot/0.3.1
    
    pytest-remotedata v0.3.1

commit f19b73fc191a8505b5c56f154dbe89eff431f990
Merge: ebaa0d8 8ebb650
Author: Thomas Robitaille (conda-forge account) <34445423+astrofrog-conda-forge@users.noreply.github.com>
Date:   Mon Oct 29 14:50:44 2018 +0000

    Merge pull request #4 from drdavella/update-v0.3.1
    
    Update recipe to reflect v0.3.1 release

commit 523fc28ff6bef4628c0bdab7cb934f4a900619d4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Oct 23 21:21:26 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.10.16

commit eae4cf602fa3625562d82f712f802f0231b3c527
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Oct 23 21:21:22 2018 +0000

    updated v0.3.1

commit 8ebb6507dea25c24c55455adc2df7f4741ee9b65
Author: Daniel D'Avella <ddavella@stsci.edu>
Date:   Tue Oct 23 16:53:48 2018 -0400

    Update recipe to reflect v0.3.1 release

commit ebaa0d824481c6bdf34b752e699cb7e599405dec
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 18:24:55 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit 6b05530016d2e1e3f972313922bb7d60177ec247
Merge: 5e09f64 c3acedd
Author: Matt Craig <mattwcraig@gmail.com>
Date:   Wed May 30 09:49:49 2018 -0500

    Merge pull request #3 from conda-forge/update-to-v0.3.0
    
    Update to reflect release of v0.3.0

commit c3acedd949c5e08b9a2cac126e71402c9be47c32
Author: Daniel D'Avella <drdavella@gmail.com>
Date:   Tue May 29 21:47:37 2018 -0400

    Update to reflect release of v0.3.0

commit 5e09f64347dc96232a57324f7b781f90f2fd07ed
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue Feb 13 17:21:23 2018 +0000

    Re-render the feedstock after CI registration.

commit f31d84195adf96b40f927767cd12f788eaf6acf5
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue Feb 13 17:20:31 2018 +0000

    Re-render the feedstock after CI registration.

commit c20c1c22595eba7b481d4368931f20e8b39879f1
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue Feb 13 17:13:17 2018 +0000

    Initial feedstock commit with conda-smithy 2.4.5.
