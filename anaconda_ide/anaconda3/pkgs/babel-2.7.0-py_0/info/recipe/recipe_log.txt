commit 749989ba8dec8441011fef47ceaa3d370e11b7b8
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon May 27 13:35:51 2019 +0000

    MNT: Re-rendered with conda-build 3.18.2, conda-smithy 3.3.4, and conda-forge-pinning 2019.05.26

commit 19d30f1d4367b71b02e428d9291557a604ff13db
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon May 27 13:35:40 2019 +0000

    updated v2.7.0

commit 9fe5c25f647a6129a06b8f5bc8756af066f56ac0
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 17:13:38 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit a40060e9119ede258b06820949e3d8254ac03170
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Jul 8 13:42:30 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.6 and pinning 2018.07.01

commit ecbf434f0346b1fdbdfb6cef4404ad33b414e17e
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Jul 8 13:42:24 2018 +0000

    add noarch

commit 2bb1b85e1835feb4ca384e40ec57c1de08725877
Author: Travis CI User <travis@example.org>
Date:   Mon May 28 17:37:55 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.5 and pinning 2018.05.22

commit 6fa2824ad5fc9d28ff1ecc19a8edfafda18fdb74
Author: Travis CI User <travis@example.org>
Date:   Mon May 28 17:37:13 2018 +0000

    updated v2.6.0

commit 036efbd49e5b41446e4dc4ad314d0c883cd1078e
Author: Travis CI User <travis@example.org>
Date:   Sat Feb 24 01:49:02 2018 +0000

    MNT: Re-rendered with conda-smithy 2.4.5

commit c11941f79cf6bad5131acf457ac8b4339a69ba48
Author: Travis CI User <travis@example.org>
Date:   Sat Feb 24 01:47:43 2018 +0000

    updated v2.5.3

commit f2392b9bce10a0c6e127a0588a6bc069468f0bb1
Author: Matt Terry <matt.terry@gmail.com>
Date:   Fri Oct 27 11:06:23 2017 -0700

    bump to 2.5.1

commit f5373ae3f6080800c5ecf3989a81842bbfec68e0
Merge: 0b0e9b4 7840b96
Author: Vlad Frolov <frolvlad@gmail.com>
Date:   Wed Aug 30 10:17:43 2017 +0300

    Merge pull request #7 from pmlandwehr/master [skip ci]
    
    MNT: Re-rendered with conda-smithy 2.3.3

commit 7840b96c6ea9e06174db1d39edf5532069940281
Author: Peter <pmlandwehr@gmail.com>
Date:   Tue Aug 29 23:23:30 2017 -0700

    MNT: Re-rendered with conda-smithy 2.3.3

commit 0b0e9b46f041fe0cb70a5f436be1f06d63d181af
Merge: 1b63405 4af8b56
Author: Vlad Frolov <frolvlad@gmail.com>
Date:   Fri Aug 18 22:42:24 2017 +0300

    Merge pull request #6 from nehaljwani/bump-version-2.4.0
    
    Bump version to 2.4.0

commit 1b63405e2da2fb6c8a7c4a58a998da984cccac59
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Fri Aug 18 06:12:26 2017 +0000

    Re-render the feedstock after CI registration.

commit 4af8b565084f61f550ceb0ccc1f4e23fd6071d70
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Thu Aug 17 21:57:45 2017 +0000

    Bump version to 2.4.0
    
    - Use sha256 instead of md5
    - Update about section
    - Add myself as maintainer

commit 4099fe6ebb8c855749d25a09f2c0e153c9309a52
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Fri Jan 20 01:36:15 2017 +0000

    MNT: Updated the feedstock for conda-smithy version 2.0.0.

commit 17308bcf91b6006f3cc869f6fc932c23d0e1e761
Author: Automatic conda-forge administrator <pelson.pub+conda-forge@gmail.com>
Date:   Tue Nov 8 08:59:41 2016 +0000

    MNT: Updated the feedstock for conda-smithy version 1.4.6. (#4) [ci skip]

commit 2ad0ced39e27ffd5f7a840aa3a2deefab314dcd3
Author: Automatic conda-forge administrator <pelson.pub+conda-forge@gmail.com>
Date:   Thu Oct 13 06:50:46 2016 +0100

    MNT: Updated the feedstock for conda-smithy version 1.3.2. (#3) [ci skip]

commit 3d9d18cc3f72fc74ab80d826a4d965bdf50a0ef6
Author: Vlad Frolov <frolvlad@gmail.com>
Date:   Mon Jun 27 09:53:47 2016 +0300

    Upgrade to 2.3.4

commit b58a5d2119d63f5ef1b15073deb84adad47f9477
Author: Automatic conda-forge administrator <pelson.pub+conda-forge@gmail.com>
Date:   Mon May 16 09:08:57 2016 +0100

    MNT: Updated the feedstock for conda-smithy version 0.10.3. (#1)
    
    [ci skip]

commit 1aa3616f11c5db3168cd77513b6a8cab71f2362e
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Apr 9 12:54:55 2016 +0000

    Re-render the feedstock after CI registration.

commit 68be73e74aa3ecbee009804fd59e0d87f4d4fda9
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Apr 9 12:54:18 2016 +0000

    Initial commit of the babel feedstock.
