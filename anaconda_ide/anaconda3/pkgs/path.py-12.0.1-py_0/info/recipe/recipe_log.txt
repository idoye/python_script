commit 6eb0bfd60075e70f5cb0e164da0586010900526e
Merge: ee80159 abf9fe3
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Apr 13 17:54:08 2019 +0530

    Merge pull request #13 from regro-cf-autotick-bot/12.0.1
    
    path.py v12.0.1

commit abf9fe3e2621501bafe11f9cd7cc6a79a2130a3d
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sat Apr 13 06:37:12 2019 +0000

    updated v12.0.1

commit ee80159bfb6bc9526e9b455cfcd46e9300759624
Merge: 44ba35c dd507fd
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Apr 13 10:27:29 2019 +0530

    Merge pull request #12 from regro-cf-autotick-bot/12.0
    
    path.py v12.0

commit dd507fd3cc9506c961a0614691e5e5e40f916050
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Sat Apr 13 04:51:57 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.3.2, and conda-forge-pinning 2019.04.12

commit aeac412a3465b4025f91af7c64b3f0696ddd0079
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Apr 13 10:19:07 2019 +0530

    Update host/run deps on python
    
    xref: https://github.com/jaraco/path.py/commit/0fdb0d4a0f7a4ec1ae582b72d0a3367568c29243

commit 5ebad786b7f0184391ba0994afb75e5611ee3ed8
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Apr 12 13:31:52 2019 +0000

    updated v12.0

commit 44ba35c2f954377646b6ff55129e73ed8328ba72
Merge: faa1032 33d4132
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Apr 13 10:10:01 2019 +0530

    Merge pull request #11 from regro-cf-autotick-bot/11.5.2
    
    path.py v11.5.2

commit 33d4132316278c54f04210c8287e98484622ad3e
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Sat Apr 13 04:33:39 2019 +0000

    MNT: Re-rendered with conda-build 3.17.8, conda-smithy 3.3.2, and conda-forge-pinning 2019.04.12

commit 944b06c07ea61c62df09607f9890f03fe4a8a3a1
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Apr 9 06:26:10 2019 +0000

    updated v11.5.2

commit faa10323c98c550faefba27368b2faa808a7ed25
Merge: 53be759 135c5eb
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Oct 13 21:03:09 2018 +0530

    Merge pull request #9 from regro-cf-autotick-bot/11.4.0
    
    path.py v11.4.0

commit 135c5eb56e11c3a8cf64ec667273c64cf03f776f
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sat Oct 13 12:53:08 2018 +0000

    Bump version to 11.5.0
    
    - Use python from host section
    - Add more runtime deps

commit 7194953fd186b0aba6ade2a776e3fb28c43781fe
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Sep 28 09:10:29 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.09.27

commit be36ae0681df2778723e17c21c2e282205b7caf6
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Fri Sep 28 09:10:24 2018 +0000

    updated v11.4.0

commit 53be7591f8c22a626c2c6aa7a096f9f175a1d930
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 17:24:33 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit 5708a23ed2bf1b0299dd644e0112e468df6736ab
Merge: cf95ef5 1549a2d
Author: jakirkham <jakirkham@gmail.com>
Date:   Fri Jun 15 00:00:51 2018 -0400

    Merge pull request #5 from regro-cf-autotick-bot/11.0.1
    
    path.py v11.0.1

commit 1549a2dd6fcb471266c1eca439950d280b98198e
Author: John Kirkham <kirkhamj@janelia.hhmi.org>
Date:   Thu Jun 14 23:47:37 2018 -0400

    Package the license file

commit 658fb6f39def158d19cc97443099692695c59eb1
Author: John Kirkham <kirkhamj@janelia.hhmi.org>
Date:   Thu Jun 14 23:46:57 2018 -0400

    Drop `importlib`
    
    We don't build for Python 2.6. So we don't need to worry about backports
    to Python 2.6.

commit bc76c9f61e1fc46aebcd09eafdfb8ef9fabf96d0
Author: Isuru Fernando <isuruf@gmail.com>
Date:   Thu Jun 14 20:53:01 2018 -0600

    Update meta.yaml

commit a4a0b03d803736eab61aa2fb5063187d5eb4308f
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Thu Jun 14 19:58:47 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.5 and pinning 2018.05.22

commit d359b4eb73dd28eaa20d39a501e5f122c7bed4fa
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Thu Jun 14 19:58:04 2018 +0000

    Add noarch:python option

commit 55c4233bc13b9337a5c5e502c769a7d688490122
Author: Travis CI User <travis@example.org>
Date:   Mon Mar 26 22:44:27 2018 +0000

    MNT: Re-rendered with conda-smithy 2.4.5

commit d2af077f2a43e78bb328dcaa62e8796c118e8cc0
Author: Travis CI User <travis@example.org>
Date:   Mon Mar 26 22:43:13 2018 +0000

    updated v11.0.1

commit cf95ef5ec6df1694b51b458760fb1173fbf45549
Merge: 37ed617 ef2902b
Author: Matt Craig <mattwcraig@gmail.com>
Date:   Fri Aug 18 00:34:26 2017 -0500

    Merge pull request #3 from nehaljwani/bump-version-10.3.1
    
    Bump version to v10.3.1

commit ef2902b7503bc79b26fa09bf01ba2be5520a1596
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Sun Aug 13 14:57:45 2017 +0000

    Bump version to 10.3.1

commit 37ed61710657d03b1c659d9351ba0fa4e4fbd043
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue Aug 15 01:20:57 2017 +0000

    Re-render the feedstock after CI registration.

commit ec6212b7db6eef2773ff157a1c2dbf7e5be3ef96
Merge: f78ec92 2a1a6f7
Author: jakirkham <jakirkham@gmail.com>
Date:   Fri Mar 3 01:48:00 2017 -0500

    Merge pull request #2 from lexual/rerender_py36
    
    MNT: Re-rendered with conda-smithy 2.1.1

commit 2a1a6f7895f9fcceed93f47018a8cc1d0c5eb7d4
Author: lexual <a.hider@bom.gov.au>
Date:   Wed Mar 1 19:46:30 2017 +1100

    MNT: Re-rendered with conda-smithy 2.1.1

commit f78ec922a1ec26e8ba848152abb83c9d88835263
Merge: e015c94 78d66bf
Author: jakirkham <jakirkham@gmail.com>
Date:   Thu Oct 6 16:42:16 2016 -0400

    Merge pull request #1 from conda-forge-admin/feedstock_rerender_master
    
    MNT: Re-render the feedstock [ci skip]

commit 78d66bf665c9f36bd4fd5b4c401565ec3e2519b9
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Thu Oct 6 10:11:29 2016 +0000

    MNT: Updated the feedstock for conda-smithy version 1.3.2.

commit e015c94e6522becf7e9ccc7e3ce4af68eafa01f1
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Aug 6 02:59:44 2016 +0000

    Re-render the feedstock after CI registration.

commit 279b111c221cd0986927e2fc6599a090f30240f3
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Aug 6 02:58:35 2016 +0000

    Initial commit of the path.py feedstock.
