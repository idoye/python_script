commit b6471a58571cda29f26693ae2842432cf1fa6312
Author: Jonathan Helmus <jjhelmus@gmail.com>
Date:   Tue Apr 23 19:02:46 2019 -0500

    7.0.0
    
    simplify recipe
    use pip to install

commit 57614d80591bd468c8eb93f0e1e08c6b012950e1
Author: Jonathan Helmus <jjhelmus@gmail.com>
Date:   Wed Mar 13 08:59:43 2019 -0500

    6.0.0

commit 038dcd96f1916ecc846dfe22ef9bf6c7528940c4
Author: Michael Sarahan <msarahan@gmail.com>
Date:   Thu Jan 17 10:24:59 2019 -0600

    5.0.0

commit 162f718003e8bbb000c8da5df9411dc145370b9a
Author: Ray Donnelly <mingw.android@gmail.com>
Date:   Fri Apr 27 01:49:33 2018 +0100

    Fix pip dependency cycle by not using pip

commit 040112493a61f3786035896e7741a2dd567d7c80
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Wed Feb 21 12:31:04 2018 -0600

    Make recipe cross compile capable

commit d7e279953bfcd673525e5e109e529a5c2d2c84ba
Author: Nehal J Wani <nehaljw.kkd1@gmail.com>
Date:   Wed Feb 21 12:30:59 2018 -0600

    Update description

commit fc302343ebc35e40b643717d92ef001a0d80249a
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 19:15:40 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit 5ef2b3234f03388716b9c0699faf7fc99fe65a6b
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jul 31 13:16:18 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.8 and pinning 2018.07.24

commit 589c1eef306bcfdf0d348091a728c6e564c92af4
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Tue Jul 31 13:15:47 2018 +0000

    updated v4.3.0

commit 33ff3b289578ea29281b83f7901a3bad1148706d
Merge: e4a8b8e 7155b49
Author: Peter M. Landwehr <plandweh@andrew.cmu.edu>
Date:   Fri Jul 13 09:35:15 2018 -0700

    Merge pull request #6 from pmlandwehr/master
    
    pypi, revert noarch

commit 7155b49d343c368daf42a5bc16d8e10f74c5db0f
Author: conda-forge-admin <pelson.pub+conda-forge@gmail.com>
Date:   Thu Jul 12 03:49:45 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.6 and pinning 2018.07.01

commit 1f98d41d4884db6c3b9cc246d54e9f67160e9244
Author: Peter M. Landwehr <plandweh@andrew.cmu.edu>
Date:   Wed Jul 11 20:48:39 2018 -0700

    pypi, revert noarch

commit e4a8b8e795b5060d459b5962889872506b4ca799
Merge: 7347fbd 60427c8
Author: Peter M. Landwehr <plandweh@andrew.cmu.edu>
Date:   Fri Jun 1 21:02:03 2018 -0700

    Merge pull request #5 from regro-cf-autotick-bot/4.2.0
    
    more-itertools v4.2.0

commit 60427c81ded08ca9dd8366cad72923566e2eb7f0
Author: Travis CI User <travis@example.org>
Date:   Thu May 24 05:09:05 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.4 and pinning 2018.05.07

commit c557ca20838fe8bb47c4b0108fc03ff6320bd317
Author: Travis CI User <travis@example.org>
Date:   Thu May 24 05:08:53 2018 +0000

    updated v4.2.0

commit 7347fbdeabca78472d734595d7968ceec98490b9
Merge: 55dc2fa 787cf28
Author: Filipe <ocefpaf@gmail.com>
Date:   Mon Feb 12 11:29:01 2018 -0800

    Merge pull request #4 from ocefpaf/update_to_4.1.0
    
    update to 4.1.0

commit 787cf289b8daa3f72a2254ead30540e94360894e
Author: Filipe Fernandes <ocefpaf@gmail.com>
Date:   Mon Feb 12 09:55:20 2018 -0800

    update to 4.1.0

commit 55dc2fa323135e983b91073a9631a76353e383f9
Merge: 99ef19a 63e6a7e
Author: Peter M. Landwehr <plandweh@andrew.cmu.edu>
Date:   Fri Jan 5 23:50:11 2018 -0800

    Merge pull request #3 from pmlandwehr/master
    
    Ticked version, regenerated if needed. (Double-check reqs!)

commit 63e6a7e21cb7340557fc0dd592b6deed1e7cf16a
Author: Peter <pmlandwehr@gmail.com>
Date:   Fri Jan 5 23:25:45 2018 -0800

    MNT: Re-rendered with conda-smithy 2.4.5

commit 12ebf29fb5eeb74f732eeb899a0e030f88362d38
Author: Peter M. Landwehr <plandweh@andrew.cmu.edu>
Date:   Fri Jan 5 23:25:24 2018 -0800

    Added noarch: python

commit c51578b7eda1b2583f43df6b2c65a5d978c690e9
Author: Peter M. Landwehr <plandweh@andrew.cmu.edu>
Date:   Fri Jan 5 22:39:56 2018 -0800

    Tick version to 4.0.1

commit 99ef19a438bedf5ed0cbc251cee8924dcfe5da17
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Sat Aug 26 21:55:38 2017 +0000

    [ci skip] [skip ci] Update anaconda token

commit 989a923629b2bbe96e27c49cead73a37810a32cc
Merge: 14fc4a5 15a2c08
Author: Peter M. Landwehr <plandweh@cs.cmu.edu>
Date:   Sat Jun 24 22:42:48 2017 -0700

    Merge pull request #2 from pmlandwehr/master
    
    Tick version

commit 15a2c084d31fac8f3191f6ebde03866efb5a73cb
Author: Peter M. Landwehr <plandweh@cs.cmu.edu>
Date:   Sat Jun 24 21:42:44 2017 -0700

    Update build reqs

commit cd5f49bdab15d821dfe4c6772acb92da1a65a509
Author: Peter M. Landwehr <plandweh@cs.cmu.edu>
Date:   Sat Jun 24 20:20:44 2017 -0700

    Tick version to 3.2.0

commit 14fc4a5c9646140937ef29a782baab1f5fa366b0
Merge: efec087 2223c5d
Author: Peter M. Landwehr <plandweh@cs.cmu.edu>
Date:   Thu May 25 16:30:47 2017 -0700

    Merge pull request #1 from pmlandwehr/master
    
    Ticked version, regenerated if needed. (Double-check reqs!)

commit 2223c5dafd8ec275a51c43ee9c2b84920104df75
Author: pmlandwehr <None>
Date:   Thu May 25 13:10:32 2017 -0700

    MNT: Updated the feedstock for conda-smithy version 2.3.1.

commit 75d5e41bebc354e2e68917d89c29fb46009becaa
Author: Peter M. Landwehr <plandweh@cs.cmu.edu>
Date:   Thu May 25 13:02:51 2017 -0700

    Tick version to 3.1.0

commit efec08739077cbc0d0eb2b9f6089eaa697e1f851
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Fri Apr 7 13:35:34 2017 +0000

    Re-render the feedstock after CI registration.

commit 3624f10ecca8c93d8df02e4789be8a49ade2b156
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Fri Apr 7 13:29:41 2017 +0000

    Initial feedstock commit with conda-smithy 2.2.2.
