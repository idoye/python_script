commit 3049630a1062f36af7a1c7a6c343eee94f63376a
Author: Jonathan Helmus <jjhelmus@gmail.com>
Date:   Mon Jun 25 14:26:08 2018 -0500

    build -> host

commit 8cf109ec5ad14ed85a7bd0e553cb0a87bf48db2a
Author: Jonathan Helmus <jjhelmus@gmail.com>
Date:   Mon Jun 25 14:25:27 2018 -0500

    allow root install on linux

commit 00c3dc04443240a9623a80ec14084005e3199f5f
Merge: 09d3ba2 06618da
Author: Thomas Kluyver <takowl@gmail.com>
Date:   Sun Aug 11 14:04:58 2019 +0100

    Merge pull request #2 from regro-cf-autotick-bot/0.4.1
    
    jeepney v0.4.1

commit 06618da402df8eedf2b2941977dedfc8ca977139
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Aug 11 12:26:39 2019 +0000

    MNT: Re-rendered with conda-build 3.18.9, conda-smithy 3.4.1, and conda-forge-pinning 2019.08.09

commit 4667aaf92e91fb490937110c46010fe36ad24883
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Sun Aug 11 12:26:24 2019 +0000

    updated v0.4.1

commit 09d3ba22d25d2996424f5e51284e3c4cc200977d
Merge: 502342d 92eb7ba
Author: Carlos Cordoba <ccordoba12@gmail.com>
Date:   Mon Sep 24 08:16:29 2018 -0500

    Merge pull request #1 from regro-cf-autotick-bot/0.4
    
    jeepney v0.4

commit 92eb7ba109d35a3fed2c348062136b3760ff5ecd
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Sep 24 11:12:00 2018 +0000

    MNT: Re-rendered with conda-smithy 3.1.12 and pinning 2018.09.20

commit 5155405a2e24361cc6c7f091f67402fa394adc3c
Author: regro-cf-autotick-bot <circleci@cf-graph.regro.github.com>
Date:   Mon Sep 24 11:11:55 2018 +0000

    updated v0.4

commit 502342d2c945986779f9114737efe0db8f785d14
Author: Travis CI User <travis@example.org>
Date:   Sat Aug 4 18:56:37 2018 +0000

    [ci skip] [skip ci] Update anaconda token

commit c4bcccec5a4a7227de1c29ac5011d54c6d1abd95
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue May 15 06:36:59 2018 +0000

    Re-render the feedstock after CI registration.

commit 385bced61337921e10f65c7b0bf8857775115eea
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue May 15 03:27:40 2018 +0000

    Re-render the feedstock after CI registration.

commit e354a09ce7dea95595c2e56875c10e3309257667
Author: Travis-CI on github.com/conda-forge/staged-recipes <conda-forge@googlegroups.com>
Date:   Tue May 15 03:26:47 2018 +0000

    Initial feedstock commit with conda-smithy 3.1.3.dev12.
